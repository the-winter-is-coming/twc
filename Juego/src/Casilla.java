import javax.swing.JLabel;

public class Casilla {
	private aliado aliado1;
	private JLabel label;
	
	public Casilla(aliado aliado1, JLabel label) {
		this.aliado1 = aliado1;
		this.label = label;
	}
	
	public aliado getAliado() {
		return aliado1;
	}
	
	public void setAliado(aliado aliado1) {
		this.aliado1 = aliado1;
	}

	public JLabel getLabel() {
		return label;
	}
}