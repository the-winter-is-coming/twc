import javax.swing.ImageIcon;

public class aliado {

	ImageIcon miIcono;
	int vida;
	int da�o;

	public aliado(String nombre) {
		switch (nombre) {
			case "Jhon Nieve":
				vida = 250;
				da�o = 30;
				miIcono = new ImageIcon("IMG\\mini_mini_jhonsnow.png");
				break;
			case "Edmure Tully":
				vida = 150;
				da�o = 25;
				miIcono = new ImageIcon("IMG\\Edmure_Tully.png");
				break;
			case "Daenerys":
				vida = 400;
				da�o = 10;
				miIcono = new ImageIcon("IMG\\daenerys.png");
				break;
			
		}
	}
	
	public int getVida() {
		return vida;
	}
	
	public ImageIcon getImagen() {
		return miIcono;
	}
	
	public void setVida(int vida) {
		this.vida = vida; 
	}
	
	public int getDa�o() {
		return da�o;
	}
}
