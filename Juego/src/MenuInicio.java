import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuInicio {
	Musica musicamenu = new Musica("Music\\opening.wav");

	private JFrame frmInicio;

	public JFrame getFrmInicio() {
		return frmInicio;
	}

	VentanaJuego vJ = new VentanaJuego(this);
	
	public void setFrmInicio(JFrame frmInicio) {
		this.frmInicio = frmInicio;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuInicio window = new MenuInicio();
					window.frmInicio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuInicio() {
		musicamenu.play();
		initialize();
	}
	private void initialize() {
		frmInicio = new JFrame();
		frmInicio.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\IMG_Inicio\\logo.png"));
		frmInicio.setTitle("Inicio");
		frmInicio.setBounds(100, 100, 1164, 767);
		frmInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmInicio.getContentPane().setLayout(null);
		
		JButton Start = new JButton("Click to Start");
		Start.setBorder(null);
		Start.setFocusPainted(false);
		Start.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		Start.setFont(new Font("Viner Hand ITC", Font.PLAIN, 20));
		Start.setContentAreaFilled(false);
		Start.setIcon(null);
		Start.setBounds(304, 417, 224, 56);
		frmInicio.getContentPane().add(Start);
		
		JLabel CopyR = new JLabel("");
		CopyR.setIcon(new ImageIcon("IMG\\IMG_Inicio\\copyR.png"));
		CopyR.setBounds(0, 658, 156, 70);
		
		frmInicio.getContentPane().add(CopyR);
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setIcon(new ImageIcon("IMG\\IMG_Inicio\\fondo.jpg"));
		lblNewLabel.setBounds(0, 0, 1148, 728);
		frmInicio.getContentPane().add(lblNewLabel);
		
		//Eventos
		Start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				vJ.getFrame().setVisible(true);
				musicamenu.stop();
				frmInicio.dispose();
			}
		});
		
	}

}
