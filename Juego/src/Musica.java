import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.JOptionPane;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class Musica {
	
	String filepath;
	InputStream music;
	AudioStream audio;
	
	public Musica(String filepath) {
		
		this.filepath = filepath;
		
		try {
			this.music = new FileInputStream(new File(this.filepath));
			this.audio = new AudioStream(music);			
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error");
		}
	}
	
	public void play() {
		AudioPlayer.player.start(audio);
	}
	
	public void stop() {
		AudioPlayer.player.stop(audio);
		try {
			this.music = new FileInputStream(new File(this.filepath));
			this.audio = new AudioStream(music);			
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error");
		}
	}
}

