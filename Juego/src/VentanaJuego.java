import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaJuego {

	JFrame frame;
	public JFrame getFrame() {
		return frame;
	}
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	public static final int COLUMNAS = 10;
	public static final int FILAS = 6;
	public static final int maximoEnemigos = 10;

	MenuInicio mI;
	JPanel panel;
	Casilla arrayCasillas[][];
	JTextField Economia;
	JTextField PosicionFila;
	JTextField txtPosicionFila;
	JTextField txtPosicionColumna;
	JTextField PosicionColumna;
	JTextField txtPersonaje;
	JComboBox Personaje;
	
	JPanel barraVidaCaja;
	JPanel botonVidaCaja;
	JProgressBar barraVida;
	
	enemigo Enemigos[];
	int nEnemigos;
	
	Image img1 = new ImageIcon("IMG\\mini_mini_jhonsnow.jpg").getImage();
	Image img2 = new ImageIcon("IMG\\Edmure_Tully.png").getImage();
	Image img3 = new ImageIcon("IMG\\daenerys.jpg").getImage();

	int Posicion_Fila;
	int Posicion_Columna;
	
	Image img = new ImageIcon("IMG\\fondo.png").getImage();
	//ControladorDamageAliado controlDamageAliado = new ControladorDamageAliado();
	//ControladorDamageEnemigo controlDamageEnemigo = new ControladorDamageEnemigo();

	public VentanaJuego(MenuInicio mI) {
		crearComponentes();
		this.mI = mI;
		Enemigos = new enemigo[maximoEnemigos];
		nEnemigos = 0;
	}

	private void crearComponentes() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setTitle("The Winter Is Coming");
		frame.setBounds(100, 100, 1142, 753);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(10, 109, 1106, 594);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(6, 1, 10, 10));

		// Dinero
		Economia = new JTextField();
		Economia.setFont(new Font("Trajan Pro 3", Font.BOLD, 12));
		Economia.setEditable(false);
		Economia.setHorizontalAlignment(SwingConstants.CENTER);
		Economia.setText("550 OROS");
		Economia.setBounds(40, 18, 97, 19);
		frame.getContentPane().add(Economia);
		Economia.setColumns(10);

		// Menu y seleccion de filas, personajes...
		PosicionFila = new JTextField();
		PosicionFila.setHorizontalAlignment(SwingConstants.CENTER);
		PosicionFila.setText("1");
		PosicionFila.setEditable(true);
		PosicionFila.setBounds(256, 48, 86, 20);
		frame.getContentPane().add(PosicionFila);
		PosicionFila.setColumns(10);

		txtPosicionFila = new JTextField();
		txtPosicionFila.setEditable(false);
		txtPosicionFila.setText("Posicion Fila");
		txtPosicionFila.setBounds(256, 17, 86, 20);
		frame.getContentPane().add(txtPosicionFila);
		txtPosicionFila.setColumns(10);

		txtPosicionColumna = new JTextField();
		txtPosicionColumna.setHorizontalAlignment(SwingConstants.CENTER);
		txtPosicionColumna.setText("Posicion Columna");
		txtPosicionColumna.setEditable(false);
		txtPosicionColumna.setColumns(10);
		txtPosicionColumna.setBounds(363, 17, 127, 20);
		frame.getContentPane().add(txtPosicionColumna);

		PosicionColumna = new JTextField();
		PosicionColumna.setHorizontalAlignment(SwingConstants.CENTER);
		PosicionColumna.setEditable(true);
		PosicionColumna.setText("1");
		PosicionColumna.setColumns(10);
		PosicionColumna.setBounds(363, 48, 86, 20);
		frame.getContentPane().add(PosicionColumna);

		txtPersonaje = new JTextField();
		txtPersonaje.setHorizontalAlignment(SwingConstants.CENTER);
		txtPersonaje.setText("Personaje");
		txtPersonaje.setEditable(false);
		txtPersonaje.setColumns(10);
		txtPersonaje.setBounds(150, 17, 86, 20);
		frame.getContentPane().add(txtPersonaje);

		Personaje = new JComboBox();
		Personaje.setModel(
		new javax.swing.DefaultComboBoxModel<>(new String[] { "Jhon Nieve", "Edmure Tully", "Daenerys" }));
		Personaje.setEditable(false);
		Personaje.setBounds(150, 48, 103, 20);
		frame.getContentPane().add(Personaje);

		JButton btnPoner = new JButton("PONER");
		btnPoner.setBounds(40, 47, 89, 23);
		frame.getContentPane().add(btnPoner);

		// VIDA
		barraVidaCaja = new JPanel();
		barraVidaCaja.setBounds(0, 70, 100, 20);
		barraVidaCaja.setBackground(Color.LIGHT_GRAY);
		
		barraVida= new JProgressBar(0, 100);
		barraVida.setPreferredSize(new Dimension(104, 30));
		barraVida.setForeground(Color.green);
		barraVida.setValue(100);
		barraVidaCaja.add(barraVida);
		
		JButton EmpezarPartida = new JButton("EMPEZAR PARTIDA");
		EmpezarPartida.setForeground(Color.WHITE);
		EmpezarPartida.setFocusPainted(false);
		EmpezarPartida.setBackground(Color.BLACK);
		EmpezarPartida.setBounds(669, 0, 207, 50);
		frame.getContentPane().add(EmpezarPartida);
		
		// Tablero de juego
		int filas = 0;
		int columnas = 0;

		arrayCasillas = new Casilla[6][10];

		for (filas = 0; filas < FILAS; filas++) {
			for (columnas = 0; columnas < COLUMNAS; columnas++) {

				JLabel campo = new JLabel();

				if (columnas == 0) {
					campo.setIcon(new ImageIcon("IMG/muro.jpg"));
				} else {
					campo.setIcon(new ImageIcon("IMG/suelonevado.jpg"));
				}
				arrayCasillas[filas][columnas] = new Casilla(null, campo);
				panel.add(campo);
			}
		}

		// fondo siempre va en ultimo lugar
		JLabel fondo = new JLabel("");
		fondo.setHorizontalAlignment(SwingConstants.CENTER);
		fondo.setIcon(new ImageIcon(img.getScaledInstance(1136, 724, Image.SCALE_SMOOTH)));
		fondo.setBounds(0, 0, 1136, 724);
		frame.getContentPane().add(fondo);
		
		// Eventos
		btnPoner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ponerAliado();
			}
		});
		EmpezarPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Figura();
				EmpezarPartida.setVisible(false);
			}
		});
	}

	public void Figura() {
		if(nEnemigos < maximoEnemigos) {
			Enemigos[nEnemigos] = new enemigo(this, 100);
			nEnemigos++;
		}
	}
	
	public aliado ocupada(int fila, int columna) {
		if(arrayCasillas[fila][columna].getLabel().getIcon() ==  arrayCasillas[Posicion_Fila][Posicion_Columna].getLabel().getIcon()) {
			return arrayCasillas[fila][columna].getAliado();
		}
		else {
			return null;
		}
		
	}

	public void pon(int fila, int columna, ImageIcon enemigo1) {
		arrayCasillas[fila][columna].getLabel().setIcon(enemigo1);
		arrayCasillas[fila][columna].getLabel().add(barraVidaCaja);
		//frame.paintAll(frame.getGraphics());
		//panel.paintAll(panel.getGraphics());
	}
	public void borra(int fila, int columna, ImageIcon suelonevado) {
		arrayCasillas[fila][columna].getLabel().setIcon(suelonevado);
		arrayCasillas[fila][columna].getLabel().remove(barraVidaCaja);
	}
	
	public void ponerAliado() {

		String personaje = Personaje.getSelectedItem().toString();
		aliado aliado1 = new aliado(personaje);
		String posicionFila = PosicionFila.getText(); 
		int PFila = (Integer.parseInt (posicionFila))-1;
		String posicionColumna = PosicionColumna.getText();
		int PCol = (Integer.parseInt (posicionColumna));
		Posicion_Fila = PFila;
		Posicion_Columna = PCol;
		
		if (PFila<0 || PFila>6 || PCol<1 ||PCol >8) {
		JOptionPane.showMessageDialog(null, "Columna o fila no v�lida", "ERROR", JOptionPane.WARNING_MESSAGE);
		}
		else {
			if (arrayCasillas[PFila][PCol].getAliado() == null) {
				String dinero = Economia.getText(); 
				String[] arrayDinero = dinero.split(" ");
				if(Integer.parseInt(arrayDinero[0]) >= 100) {
					int oros = Integer.parseInt(arrayDinero[0]) - 100;
					Economia.setText(oros + " OROS");
	
					arrayCasillas[PFila][PCol].setAliado(aliado1);
					arrayCasillas[PFila][PCol].getLabel().setIcon(aliado1.getImagen());
					//arrayCasillas[PFila][PCol].getLabel().add(barraVidaCaja);
				
				} else {
					JOptionPane.showMessageDialog(null, "Te has quedado sin dinero compa�ero","ERROR", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "No puedes poner un aliado en una celda ocupada","ERROR", JOptionPane.WARNING_MESSAGE);
		}
	}
}
	
	public void sumarOro(int cash) {
		String dinero = Economia.getText(); 
		String[] arrayDinero = dinero.split(" ");
		
		int oros = Integer.parseInt(arrayDinero[0]) + cash;
		Economia.setText(oros + " OROS");
	}
	public void actualizaVida(int vidaEnemigo) {
		barraVida.setValue(vidaEnemigo);
		frame.paintAll(frame.getGraphics());
		panel.paintAll(panel.getGraphics());
	}
	public void reiniciaVida(int vida) {
		barraVida.setValue(vida);
		frame.paintAll(frame.getGraphics());
		panel.paintAll(panel.getGraphics());
	}
}
