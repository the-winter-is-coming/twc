

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class enemigo extends Thread{

	private VentanaJuego mijuego;
	JPanel barraVidaCajaEnemigo;
	JProgressBar barraVidaEnemigo;
	int tipo;
	ImageIcon miIcono;
	int velocidad;
	int vida;
	int da�o;
	int randomFila;

	public enemigo(VentanaJuego juego, int Vida) {
		mijuego = juego;
		tipo = (int) (Math.random()*3);
		randomFila = (int) (Math.random()*6);
		vida = Vida;
		switch (tipo) {
			case 0:
				miIcono = new ImageIcon("IMG/enemigo1.png");
				velocidad = 2000;
				da�o = 10;
				break;
			case 1:
				miIcono = new ImageIcon("IMG/enemigo2.jpg");
				velocidad = 1200;
				da�o = 20;
				break;
			case 2:
				miIcono = new ImageIcon("IMG/enemigo3.jpg");
				velocidad = 800;
				da�o = 30;
				break;
		}
		this.start();
	}
	
	public void run() {			
		
			for (int cnt = 9; cnt > 0; cnt--) {
				aliado aliado1 = mijuego.ocupada(randomFila,cnt);
				
				if (aliado1 != null) {
					int vidaAliado = aliado1.getVida();
					int da�oAliado = aliado1.getDa�o();
					aliado1.setVida(vidaAliado - da�o);
					this.vida = this.vida - da�oAliado;
					int vidaEnemigoN = getVida();
					mijuego.actualizaVida(vidaEnemigoN);

					cnt++;
					if (this.vida <= 0) {
						mijuego.borra(randomFila, cnt, new ImageIcon("IMG/suelonevado.jpg"));
						mijuego.sumarOro(100);
						mijuego.reiniciaVida(vida = 100);
						mijuego.Figura();
						break;
						
					}
				}
				else {
					if (cnt < 9) {
						mijuego.borra(randomFila, cnt+1, new ImageIcon("IMG/suelonevado.jpg"));
					}
					mijuego.pon(randomFila, cnt, miIcono);
				}
				try {
					Thread.sleep(velocidad);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
	}
	
	public int getVida() {
		return vida;
	}
	
	public void setVida(int vida) {
		this.vida = vida; 
	}
}
